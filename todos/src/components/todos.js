import { useState, useEffect } from 'react';
import './index.css'

import { MdAddBox, MdDelete } from 'react-icons/md'

function Todos() {
    const [todos, updatetodos] = useState([])
    const [todoInput, updateTodoInput] = useState("")

    useEffect(() => {
        fetchtodos()
    }, [])

    async function fetchtodos() {
        const response = await fetch('http://127.0.0.1:8000/todos')
        const data = await response.json()
        updatetodos(data.todolist)
    }

    async function updateTodoStatus(iscompleted, id) {
        const body = JSON.stringify({
            iscompleted: !iscompleted
        })
        const response1 = await fetch(`http://127.0.0.1:8000/todos/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: body
        })
        const data1 = await response1.json()
        let newtodoList = todos.map((todo) => {
            if (todo.id === id) {
                todo.iscompleted = data1.updatedTodo.iscompleted
            }
            return todo
        })
        updatetodos(newtodoList)
    }

    async function deleteTodo(id) {
        const response1 = await fetch(`http://127.0.0.1:8000/todos/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
        })
        const data1 = await response1.json()
        console.log(data1)
        updatetodos(todos.filter((todo) => todo.id !== data1.deletedTodo.id))
    }

    async function addNewTodo() {
        if (todoInput === "") {
            return null
        }
        const body = JSON.stringify({
            title: todoInput,
            iscompleted: false
        })
        const response1 = await fetch('http://127.0.0.1:8000/todos', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: body
        })
        const data1 = await response1.json()
        updateTodoInput("")
        updatetodos([...todos, data1.newTodoItem])
    }

    let noOfTOdosCOmpleted = todos.filter((todo) => todo.iscompleted).length


    return (
        <div className='todo-page'>
            <div className='todo-container'>
                <div className='input-container'>
                    <div className='input-todo-button-container'>
                        <input className='input' type="text" placeholder="Add new Todo.." onChange={(event) => updateTodoInput(event.target.value)} value={todoInput} />
                        <button type="button" onClick={addNewTodo} className='add-todo-button'>
                            <MdAddBox className='add-todo-button-icon' />
                        </button>
                    </div>

                </div>
                <div className='todo-list-container-with-title'>
                    <h1 style={{ textAlign: 'left' }}>Todo's</h1>
                    <h3 style={{ textAlign: 'left' }}>{todos.length} Total {noOfTOdosCOmpleted} Completed and {todos.length - noOfTOdosCOmpleted} Pending</h3>
                    <div className="todolist">
                        <div className="todo-header-column">
                            <p className='todo-checkbox'>#</p>
                            <h3 className='todo-title'>Todo title</h3>
                            <p className='todo-status'>Status</p>
                        </div>
                        {todos.length > 0 && (
                            todos.map(todo => {
                                let checkedTodoStyle = todo.iscompleted ? " checked" : ""
                                return (
                                    <div key={todo.id} id={todo.id} className={"todo-item" + checkedTodoStyle}>
                                        <input id={todo.title} type="checkbox" checked={todo.iscompleted}
                                            onChange={() => updateTodoStatus(todo.iscompleted, todo.id)} className='todo-checkbox' />
                                        <label htmlFor={todo.title}>
                                            <h3 className='todo-title'>{todo.title}</h3>
                                        </label>
                                        <p className='todo-status'>{todo.iscompleted ? "Complete" : "Pending"}</p>
                                        <button type="button" onClick={() => deleteTodo(todo.id)} className='delete-todo-button'>
                                            <MdDelete className='delete-todo-button-icon' />
                                        </button>
                                    </div>)
                            })
                        )}
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Todos