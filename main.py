from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session
import schema
from database import SessionLocal, engine
import model
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import schema
from typing import Optional

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://127.0.0.1",
    "http://127.0.0.1:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

model.Base.metadata.create_all(bind=engine)

def get_database_session():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

@app.get("/todos/{todo_id}")
@app.get("/todos")
async def read_item(todo_id: Optional[int] = None,  db: Session = Depends(get_database_session)):
    if todo_id is not None:
        todo = db.query(model.Todos).filter(model.Todos.id == todo_id).first()
        return {"todoItem": todo}
    else:
        todos = db.query(model.Todos).all()
        return {"todolist": todos}


@app.post("/todos")
async def create_item(item: schema.TodosCreate, db: Session = Depends(get_database_session)):
    todo = model.Todos(title=item.title, iscompleted=item.iscompleted)
    db.add(todo)
    db.commit()
    db.refresh(todo)
    return {"newTodoItem": todo}


@app.put("/todos/{todo_id}")
async def update_todo(todo_id: int, todo: schema.TodosUpdate, db: Session = Depends(get_database_session)):
    existing_todo = db.query(model.Todos).filter(model.Todos.id == todo_id).first()
    if existing_todo is None:
        raise HTTPException(status_code=404, detail="Todo not found")
    for field, value in todo.dict(exclude_unset=True).items():
        setattr(existing_todo, field, value)
    db.add(existing_todo)
    db.commit()
    db.refresh(existing_todo)
    return {"updatedTodo": existing_todo}


@app.delete("/todos/{todo_id}")
async def delete_todo(todo_id: int, db: Session = Depends(get_database_session)):
    todo = db.query(model.Todos).filter(model.Todos.id == todo_id).first()
    if todo is None:
        raise HTTPException(status_code=404, detail="Todo not found")
    db.delete(todo)
    db.commit()
    return {"deletedTodo": todo}