from pydantic import BaseModel
from typing import Optional

class Todos(BaseModel):
    title: str
    iscompleted: Optional[bool] = False

class TodosUpdate(BaseModel):
    title: Optional[str] = None
    iscompleted: Optional[bool] = None

class TodosCreate(Todos):
    pass