from sqlalchemy.schema import Column
from sqlalchemy.types import String, Integer, Text,Boolean
from database import Base
class Todos(Base):
    __tablename__ = "Todos"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(20), unique=True)
    iscompleted=Column(Boolean)
   



